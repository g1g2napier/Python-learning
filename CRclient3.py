#!/usr/bin/python
'''
Grant Napier, Sr.
Deluxe Chatroom Client Program
CS498G
Written in and for python 2.7
To run program from command line type:   python chat2.py

Code is adapted from:
http://www.binarytides.com/code-telnet-client-sockets-python/

'''

# telnet program example
import socket, select, string, sys
from colorama import init, Fore, Back, Style
init(autoreset=True)


#main function
if __name__ == "__main__":
     
    if(len(sys.argv) < 3) :
        print 'Usage : python CRclient3.py hostname port'
        sys.exit()
     
    host = sys.argv[1]
    port = int(sys.argv[2])
     
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
     
    # connect to remote host
    try :
        s.connect((host, port))
    except :
        print 'Unable to connect'
        sys.exit()
     
    print ( Fore.BLACK + Back.YELLOW + 'Connected to remote host')
    username = raw_input( Fore.BLUE + Back.WHITE + "Enter your username: ") 
    print ( Fore.MAGENTA + Back.CYAN + "Type you messages and then hit enter.")
    
    while 1:
        socket_list = [sys.stdin, s]
         
        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
         
        for sock in read_sockets:
            msg = "::: "
            #incoming message from remote server
            if sock == s:
                data = sock.recv(4096)
                if not data :
                    print ( Fore.RED + Back.GREEN + "Connection closed. \n Later, suckers! ")
                    sys.exit()
                else :
                    #print data
                    #sys.stdout.write(Fore.WHITE + Back.BLACK + data)
                    print(data)             

            #user entered a message
            else :
                msg = raw_input( Fore.CYAN + Back.BLACK + ": ") 
                msg = ( username + " Says: " + msg + "\n" )
                s.send(msg)


