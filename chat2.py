'''
Grant Napier, Sr.
Deluxe Chat Program
CS498G
Written in and for python 2.7
To run program from command line type:   python chat2.py

Improvements over the chat program in the book:
1) Allowed users to input their user name.
2) Allowed users to see the user name that typed the message.
3) Allowed server side to be able to create and record a log of the chat.
4) Added color to both user and server modules.
5) Allowed server to change port number of user automatically.

'''
from __future__ import print_function
import os
import sys
import socket
from colorama import init, Fore, Back, Style
init(autoreset=True)


def server():
    global port, user_name, cl_user_name
    host = "localhost"
    user_name = raw_input( Fore.BLACK
						 + Back.YELLOW
						 + "Enter your username: ")  
	# added username prompt
    #print (user_name)   
    filename = raw_input( Fore.BLACK +
                         Back.YELLOW +
                         "Enter full file name including .txt for chat log (blank to not save log)")
    if filename == "":
        log = False
    else:
        log_file = open(filename, "a+")
        log = True
   # print (log)
    
    comms_socket = socket.socket()
    comms_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    comms_socket.bind((host, port))
    comms_socket.listen(10)
    print ( Fore.RED
		  + Back.GREEN
          + "WARNING: ONLY ONE LINE OF TEXT CAN BE TRANSMITTED OR RECIEVED AT ONE TIME!!! \n To exit program press CONTROL + C  ")
    #init(autoreset=False)
    print (Fore.BLACK + Back.YELLOW +"Waiting for a chat at ", host, " on port ", port)
    send = ""

    while True:
        connection, address = comms_socket.accept()
        print (Fore.BLACK + Back.YELLOW + "opening chat with ", address)
 
        while True:
            recieved = ( connection.recv(4096).decode("UTF-8"))
            if log == True:
                log_file.write(recieved + "\n")
            print (Fore.WHITE + Back.BLUE + recieved )
            send = raw_input( Fore.WHITE + Back.RED + user_name + ": ")
            send = str(send)
            s = send[0]
            if s == "!q":
                sys.exit()
            send_data = (user_name + " Says: " + send)
            connection.send(bytes(send_data).encode("UTF-8"))
            if log == True:
                log_file.write(send_data + "\n")

        send_data = ""
        connection.close()
    if log == True:
        log_file.flush()
        log_file.close()
# ########### END SERVER


def client():
    global port, user_name, cl_user_name
    host = raw_input ( Fore.BLACK 
                     + Back.YELLOW 
                     + "Enter the host you want to communicate with (leave blank for localhost) ")
    if host == "":
        host = "localhost"
    cl_user_name = raw_input( Fore.BLACK + Back.YELLOW + "Enter your username: ") 
  #  print (cl_user_name)
    comms_socket = socket.socket()
   # comms_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


    print ( Fore.RED
		  + Back.GREEN
          + "WARNING: ONLY ONE LINE OF TEXT CAN BE TRANSMITTED OR RECIEVED AT ONE TIME!!! \n To exit program press CONTROL + C ")
    print (Fore.BLACK + Back.YELLOW + "Starting a chat with ", host, " on port ", port)
    comms_socket.connect((host, port))
    #init(autoreset=False)
    while True:
        send = raw_input(Fore.WHITE + Back.BLUE + cl_user_name + ": ")
        send = str(send)
        s = send[0]
        if s == "q":
            sys.exit()
        send_data = (cl_user_name + " says: " + send)
        comms_socket.send(bytes(send_data).encode("UTF-8"))

        received = comms_socket.recv(4096).decode("UTF-8")
        print ( Fore.WHITE + Back.RED + received)

# ##########  END CLIENT
"""
print(Fore.GREEN + 'green, '
    + Fore.RED + 'red, '
    + Fore.RESET + 'normal, '
    , end='')
print(Back.GREEN + 'green, '
    + Back.RED + 'red, '
    + Back.RESET + 'normal, '
    , end='')
print(Style.DIM + 'dim, '
    + Style.BRIGHT + 'bright, '
    + Style.NORMAL + 'normal'
    , end=' ')
print()
"""
#  Start ****************************************************************
port_in = raw_input ( Fore.BLACK 
                    + Back.YELLOW 
                    + "Enter the port you want to communicate on: \n(0 or blank for default):")
# These next ten lines allows port input to be null or None or ""
# and ensures that the input meets some criteria, and if not, sets it to the default value.
port_in = unicode(port_in, 'utf-8',)  # port_in must be unicode for isnumeric test to work.
if len(port_in) < 4:
    port_in = 0
elif port_in is "":
    port_in = 0
elif not port_in.isnumeric():
    port_in = 0
else:
    port_in = int(port_in)

# These lines enforce that tne port input is in a proper range, and if not sets it to default value.
port_in = int(port_in)
port = port_in
if port < 100 or port > 65535:
    port = 50000
else:
    port = port
while True:
    print (Fore.WHITE + Back.RED +"Your options are:")
    print (Fore.RED + Back.BLUE +"1 - wait for a chat")
    print (Fore.GREEN + Back.WHITE +"2 - initiate a chat")
    print (Fore.YELLOW + Back.BLACK +"3 - exit")

    option = int(raw_input("option :"))

    if option == 1:
        server()
    elif option == 2:
        client()
    elif option == 3:
        break
    else:
        print (Fore.BLACK + Back.YELLOW +"I don't recognize that option. Please try again.")


