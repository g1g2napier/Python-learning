#!/usr/bin/python
'''
Grant Napier, Sr.
Deluxe system information programming tool
CS498G
Written in and for python 2.7
To run program from command line type:   python sysinfo.py

Code is adapted from many sources.

'''
import platform
import socket
from datetime import datetime
from uuid import getnode as get_mac
mac = get_mac()
platform.node()
socket.gethostname()

print 'Version      :', platform.python_version()
print 'Version tuple:', platform.python_version_tuple()
print 'Compiler     :', platform.python_compiler()
print 'Build        :', platform.python_build()

print 'Normal :', platform.platform()
print 'Aliased:', platform.platform(aliased=True)
print 'Terse  :', platform.platform(terse=True)

print 'uname:', platform.uname()

print 'system   :', platform.system()
print 'node     :', platform.node()
print 'release  :', platform.release()
print 'version  :', platform.version()
print 'machine  :', platform.machine()
print 'processor:', platform.processor()

print 'interpreter:', platform.architecture()
print '/bin/ls    :', platform.architecture('/bin/ls')
print ( socket.gethostname() )

print ( "Date and Time: " )
print ( datetime.now() )

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
print ('mac address:', mac)
print (mac)
print ( s.getsockname()[0])
ip = s.getsockname()[0]
print ( "IP address: ", ip )
s.close()


