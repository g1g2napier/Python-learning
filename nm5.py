'''
Network Monitoring Tool
Gets the ip address, mac address,
device manufacturer, and guesses the operating system

Written for CS498G at Hood College
Joshua C., Grant N., Phil H.

Dependencies:
Must run python command with root privlages for nmap

Must have nmap command line utility installed

Must have arp-scan command line utility installed

Must have netifaces installed for python

'''

import subprocess
import re
import platform
import netifaces
import os
import sys

# Returns the list of IP addresses on the chosen interface for Linux
# Returns the list of IP addresses on the default connected interface for Windows
def get_ips():
	
	# Get platform to check which command to run to get IP addresses
	myplatform = platform.system()
	interfaces = netifaces.interfaces()
	devnull = open(os.devnull, 'w')
	arg2 = ""

	# Lets the user pick which interface to check for
	# If the given interface isn't found, it will choose the default connected interface
	if(myplatform == 'Linux'):
		scan_type = raw_input("\nWould you like to look for devices on your local wired or wireless network?"
							 + "\nEnter 'e' for wired or 'w' for wireless: ")
		
		# This condition chooses the wireless wlan interface for linux machines
		if(scan_type == 'w'):
			for interface_type in interfaces:
				if(interface_type.startswith('w')):
					arg2 = interface_type
			if(arg2 == ""):
				print "Could not find wireless interface, scanning " + interfaces[1] + "."
				arg2 = interfaces[1]			
		
		# This condition chooses the wired eth interface for linux machines
		elif(scan_type == 'e'):
			for interface_type in interfaces:
				if(interface_type.startswith('e')):
					arg2 = interface_type
			if(arg2 == ""):
				print "Could not find wired interface, scanning " + interfaces[1] + "."
				arg2 = interfaces[1]

		# Finishes our linux command line argument
		arparg = ['arp-scan', '--interface='+arg2, '--localnet']
	else:
		arparg = ['arp', '-a']

	# Calls the argument above and pipes the output back into our program
	arpng = subprocess.Popen((arparg), stdout=subprocess.PIPE, stderr=devnull)
	ipout = arpng.communicate()[0]

	# initalize an empty list to fill with just IP addresses
	# compiles our regular expression to check each lines against
	# and then add that line to our list of IPs if it is a match
	iplist = []
	regexp = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
	
	# use a loop and a regex to extract just the IP addresses
	for lines in ipout.splitlines():
		if regexp.search(lines):
			iplist.append(re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', lines).group())
	return iplist

# Input: A list of ip addresses on your local network
def device_info(iplist):
	device_list = []
	current_ip = ""
	current_mac = ""
	current_manuf = ""
	current_os = ""
	nmap_scantime = ""
	devnull = open(os.devnull, 'w')

	# Performs an nmap scan for each IP address given to it
	for addr in iplist:

		# Calls the nmap command with each IP address and pipes the output back into our pogram
		args = ['nmap', '-O', '-v', addr]
		nmap = subprocess.Popen((args), stdout=subprocess.PIPE, stderr=devnull)
		nmout = nmap.communicate()[0]
		current_ip = addr

		# Used to split the output of our nmap command into easily readable results
		for lns in nmout.splitlines():
			if(lns.startswith("MAC")):
				current_mac = lns.split("(")[0]
				current_manuf = lns[lns.find("(")+1:lns.find(")")]
			if lns.startswith("Aggressive OS guesses:"):
				current_os = lns.split(":")[1]
			if lns.startswith("Nmap done:"):
				rescan = re.findall("\d+\.\d+",lns)
				nmap_scantime = rescan[0]
		if current_os == "":
				current_os = 'Too many fingerprints match this host to give specific OS details'
		
		# Output device info and scan time
		print '\n------ Device Info ------'
		print 'IP Address: ' + current_ip
		print 'Mac Address: ' + current_mac
		print 'Device Manufacturer: ' + current_manuf
		print 'Operating System Guesses:' + current_os
		print 'Device Scan time: ' + nmap_scantime + ' seconds\n'

# Main function
# Calls get_ips to get a list of IP addresses
# Prints the IP addresses
# Passes IP addresses into device_info to get
# and print device info
if __name__ == '__main__':
	go = 0
	while go is 0:
		ips = get_ips()
		print '\nIP Addresses:'
		for ip in ips:
			print ip
		device_info(ips)
		go += 1
	go = raw_input("To run again, press 0.\nTo quit, press any other key.")