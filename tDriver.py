'''
Grant Napier, Sr.
Programming Tools Driver
CS498G
Written in and for python 2.7
To run program from command line type:   python tDriver.py

This program opens each of the programming tools that we have implemented.
'''
#!/usr/bin/python
from __future__ import print_function
import os
import sys
from colorama import init, Fore, Back, Style
init(autoreset=False)

import subprocess
import shlex


while True:
    print ( Fore.WHITE + Back.BLUE +" ***** WELCOME! ***** WELCOME! ***** WELCOME! *****\n"
          +" These are the tools that we have built! \n"
          + Fore.BLACK + Back.BLUE 
          +" Please, hold all questions until the end of the presentation!!\n"
          +" You may feel free to applaud at any time!\n"
          + Fore.WHITE + Back.BLUE 
          +" Please, select each TOOL by the number preceding the TOOL: \n\n")
    print ( Fore.WHITE + Back.RED 
          +" #1 = The TOOL to get SYSTEM INFORMATION about the current system!\n"
          +" #2 = The TOOL to start a CHAT with another system!\n"
          +" #3 = The TOOL to start an " + Fore.BLACK + Back.WHITE + "ASYNCRONOUS CHAT ROOM!!\n"
          + Fore.WHITE + Back.RED 
          +" #4 = The TOOL to GET NETWORK INFORMATION about the current network!\n"
          +" #5 = The TOOL to PING ANOTHER SYSTEM TO DEATH!\n"
          +" ________press CONTROL + C to exit any tool. _______________________\n")

    option = int(raw_input( Back.WHITE + Fore.RED + " YoUr OpTiOn :\n"))

    if option == 1:
        print ( Fore.RED + Back.GREEN +"option = 1\n")
        os.system('python sysinfo.py')
    
    elif option == 2:
        print ( Fore.WHITE + Back.BLACK +"option = 2\n")
        print ( "A second terminal window should open for demonstration purposes.")
        # subprocess.Popen(shlex.split('gnome-terminal -x bash -c "ls; read -n1"'))
        # THIS IS HOW WE DO IT! BABY!
        subprocess.Popen(shlex.split('gnome-terminal -x bash -c "python chat2.py"'))
        os.system('python chat2.py')
    
    elif option == 3:
        print ( Fore.BLACK + Back.GREEN +"option = 3\n")
        print ( "Several terminal windows should open for demonstration purposes.")
        subprocess.Popen(shlex.split('gnome-terminal -x bash -c "python CRclient3.py localhost 5006"'))
        print (Fore.BLACK + Back.GREEN +"Client #1 should be open.\n")
        subprocess.Popen(shlex.split('gnome-terminal -x bash -c "python CRclient3.py localhost 5006"'))
        print (Fore.BLACK + Back.GREEN +"Client #2 should be open.\n")
        #subprocess.Popen(shlex.split('gnome-terminal -x bash -c "python CR3.py"'))
        print (Fore.BLACK + Back.GREEN +"Server should be open below.\n")
        os.system('python CR3.py')

    elif option == 4:
        print ( Fore.GREEN + Back.BLACK +"option = 4\n")
        #os.system('sudo python nm3.py')
        #subprocess.Popen(shlex.split('gnome-terminal -x bash -c "sudo python nm5.py"'))
        os.system('sudo python nm5.py')
    elif option == 5:
        print ( Fore.CYAN + Back.BLACK +"option = 5\n")
        print (Fore.CYAN + Back.BLACK +"Ping Of Death should be open in new terminal window.\n")
        os.system('sudo python pingtd.py')
    else:
        print (Fore.BLACK + Back.YELLOW +"I don't recognize that option. Please try again.")


