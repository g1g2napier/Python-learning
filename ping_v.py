import sys
from platform import system as system_name # Returns the system/OS name
from os import system as system_call       # Execute a shell command

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that some hosts may not respond to a ping request even if the host name is valid.
    """

    # Ping parameters as function of OS
    parameters = "-n 1" if system_name().lower()=="windows" else "-c 1"

    # Pinging
    #return system_call("ping " + parameters + " " + host) == 0
    if system_call("ping " + parameters + " " + host) == 0:
        return True

if __name__ == '__main__':

#    target = raw_input("Enter the target address to verify: ")
#    if not target:
#        target = "192.168.1.79"

    if(len(sys.argv) < 2) :
        print 'Usage : python ping_v.py hostname'
        sys.exit()

    target = sys.argv[1]

    go = 0
    times = 0
    while go < 5:
        if ping(target) is not True:
            print ( "\n\n The target is down. It failed to ping ", go, " out of ", times, "times from here!\n\n")
            go += 1
        else:
            times += 1
#            print ( " The target is up. ")
#            go += 1

    go = raw_input("Press enter to exit.")


