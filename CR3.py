#!/usr/bin/python
'''
Grant Napier, Sr.
Deluxe Chat Room Server Program
CS498G
Written in and for python 2.7
To run program from command line type:   python CR3.py

Code is adapted from:
http://www.newthinktank.com/2010/11/python-2-7-tutorial-pt-18-chat-server/

'''
from asyncore import dispatcher
from asynchat import async_chat
import socket, asyncore
from colorama import init, Fore, Back, Style
init(autoreset=False)

PORT = 5006
NAME = 'ChatLine'

class ChatSession(async_chat):
    def __init__(self,server,sock):
        async_chat.__init__(self, sock)
        self.server = server
        self.set_terminator('\n')
        self.data = []

    def collect_incoming_data(self, data):
        self.data.append(data)

    def found_terminator(self):
        line = ''.join(self.data)
        self.data = []
        self.server.broadcast(line)

    def handle_close(self):
        async_chat.handle_close(self)
        self.server.disconnect(self)

class ChatServer(dispatcher):
    def __init__(self, port, name):
        dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(('',port))
        self.listen(15)
        self.name = name
        self.sessions = []

    def disconnect(self, sessions):
        self.sessions.remove(session)

    def broadcast(self, line):
        ccount = 0
        for session in self.sessions:
            ccount += 1 
            if ccount % 2 is 0:                
                fcolor = Fore.WHITE
                bcolor = Back.BLUE
            else:
                fcolor = Fore.BLACK
                bcolor = Back.RED

            session.push( fcolor + bcolor + "> " + line )
        mpline = (" Message pushed: " + "><> " + line )
        print (mpline)
        
        try:
            log_file = open('CR3log.txt', 'a+')
            log_file.write( mpline + '\n' )
            log_file.flush()
            log_file.close()
        except:
            print ("logging failure")
        #smsg = raw_input(Fore.RED + Back.GREEN + "><> ")
        #session.push(smsg)        

    def handle_accept(self):
        conn, addr = self.accept()
        self.sessions.append(ChatSession(self, conn))

if __name__ == '__main__':

    s = ChatServer(PORT, NAME)

    try: asyncore.loop()

    except KeyboardInterrupt: print




